[DSCLocalConfigurationManager()]
configuration LCMConfig
{
    Node localhost
    {
        Settings
        {
            RefreshMode                    = 'Push'
            ActionAfterReboot              = 'ContinueConfiguration'
            ConfigurationMode              = 'ApplyandAutoCorrect'
            ConfigurationModeFrequencyMins = 60
            RebootNodeIfNeeded             = $true
        }

        PartialConfiguration CIS_WindowsServer2019_v110
        {
            Description = 'CIS_WindowsServer2019_v110'
            RefreshMode = 'Push'
        }
    }
}

LCMConfig -OutputPath "C:\Configs\LCM"
