if ((Get-Module Pester).Version -lt [version]'5.0.4')
{
    Install-Module -Name Pester -Force -SkipPublisherCheck
}
Import-Module Pester
Import-Module ((Get-Module SecurityPolicyDsc -ListAvailable).ModuleBase + "\Modules\SecurityPolicyResourceHelper\SecurityPolicyResourceHelper.psm1")

BeforeAll {
    function Get-LocalAdmin
    {
        Process
        {
            Try
            {
                Add-Type -AssemblyName System.DirectoryServices.AccountManagement
                $PrincipalContext = New-Object System.DirectoryServices.AccountManagement.PrincipalContext([System.DirectoryServices.AccountManagement.ContextType]::Machine)
                $UserPrincipal = New-Object System.DirectoryServices.AccountManagement.UserPrincipal($PrincipalContext)
                $Searcher = New-Object System.DirectoryServices.AccountManagement.PrincipalSearcher
                $Searcher.QueryFilter = $UserPrincipal
                $Searcher.FindAll() | Where-Object { $_.Sid -Like "*-500" }
            }
            Catch
            {
                Write-Warning -Message "$($_.Exception.Message)"
            }
        }
    }

    $memberServer = (Get-WmiObject -Class Win32_ComputerSystem).PartOfDomain
    $domainController = $false

    $currentSecurityPolicy = Get-SecurityPolicy -Area SECURITYPOLICY
    $currentUserRightsPolicy = Get-SecurityPolicy -Area USER_RIGHTS
}

Describe "1 Account Policies" {
    Context "1.1 Password Policy" {
        It "1.1.1 (L1) Ensure 'Enforce password history' is set to '24 or more password(s)'" { $currentSecurityPolicy.'System Access'.PasswordHistorySize -as [int] | Should -BeGreaterOrEqual 24 }
        It "1.1.2 (L1) Ensure 'Maximum password age' is set to '60 or fewer days, but not 0'" { $currentSecurityPolicy.'System Access'.MaximumPasswordAge -as [int] | Should -BeGreaterOrEqual 60 }
        It "1.1.3 (L1) Ensure 'Minimum password age' is set to '1 or more day(s)'" { $currentSecurityPolicy.'System Access'.MinimumPasswordAge -as [int] | Should -BeGreaterOrEqual 1 }
        It "1.1.4 (L1) Ensure 'Minimum password length' is set to '14 or more character(s)'" { $currentSecurityPolicy.'System Access'.MinimumPasswordLength -as [int] | Should -BeGreaterOrEqual 14 }
        It "1.1.5 (L1) Ensure 'Password must meet complexity requirements' is set to 'Enabled'" { [System.Convert]::ToBoolean($currentSecurityPolicy.'System Access'.PasswordComplexity -as [int]) | Should -BeTrue }
        It "1.1.6 (L1) Ensure 'Store passwords using reversible encryption' is set to 'Disabled'" { [System.Convert]::ToBoolean($currentSecurityPolicy.'System Access'.ClearTextPassword -as [int]) | Should -BeFalse }
        It "1.2.1 (L1) Ensure 'Account lockout duration' is set to '15 or more minute(s)'" { $currentSecurityPolicy.'System Access'.LockoutDuration -as [int] | Should -BeGreaterOrEqual 15 }
        It "1.2.2 (L1) Ensure 'Account lockout threshold' is set to '10 or fewer invalid logon attempt(s), but not 0'" { $currentSecurityPolicy.'System Access'.LockoutBadCount -as [int] | Should -BeGreaterThan 0 }
        It "1.2.3 (L1) Ensure 'Reset account lockout counter after' is set to '15 or more minute(s)'" { $currentSecurityPolicy.'System Access'.ResetLockoutCount -as [int] | Should -BeGreaterOrEqual 15 }
    }
}

Describe "2 Local Policies" {
    Context "2.2 User Rights Assignment" {
        It "2.2.1 (L1) Ensure 'Access Credential Manager as a trusted caller' is set to 'No One'" { $currentUserRightsPolicy.SeTrustedCredManAccessPrivilege | Should -BeNullOrEmpty }
        It "2.2.4 (L1) Ensure 'Act as part of the operating system' is set to 'No One'" { $currentUserRightsPolicy.SeTcbPrivilege | Should -BeNullOrEmpty }
        It "2.2.6 (L1) Ensure 'Adjust memory quotas for a process' is set to 'Administrators, LOCAL SERVICE, NETWORK SERVICE'" { $currentUserRightsPolicy.SeIncreaseQuotaPrivilege | Should -Be @('NT AUTHORITY\LOCAL SERVICE', 'NT AUTHORITY\NETWORK SERVICE', 'BUILTIN\Administrators') }
        It "2.2.7 (L1) Ensure 'Allow log on locally' is set to 'Administrators'" { $currentUserRightsPolicy.SeInteractiveLogonRight | Should -Be @('BUILTIN\Administrators', 'BUILTIN\Users', 'BUILTIN\Backup Operators') }
        It "2.2.10 (L1) Ensure 'Back up files and directories' is set to 'Administrators'" { $currentUserRightsPolicy.SeBackupPrivilege | Should -Be @('BUILTIN\Administrators', 'BUILTIN\Backup Operators') }
        It "2.2.11 (L1) Ensure 'Change the system time' is set to 'Administrators, LOCAL SERVICE'" { $currentUserRightsPolicy.SeSystemtimePrivilege | Should -Be @( 'NT AUTHORITY\LOCAL SERVICE', 'BUILTIN\Administrators') }
        It "2.2.12 (L1) Ensure 'Change the time zone' is set to 'Administrators, LOCAL SERVICE'" { $currentUserRightsPolicy.SeTimeZonePrivilege | Should -Be @( 'NT AUTHORITY\LOCAL SERVICE', 'BUILTIN\Administrators') }
        It "2.2.13 (L1) Ensure 'Create a pagefile' is set to 'Administrators'" { $currentUserRightsPolicy.SeCreatePagefilePrivilege | Should -Be @('BUILTIN\Administrators') }
        It "2.2.14 (L1) Ensure 'Create a token object' is set to 'No One'" { $currentUserRightsPolicy.SeCreateTokenPrivilege | Should -BeNullOrEmpty }
        It "2.2.15 (L1) Ensure 'Create global objects' is set to 'Administrators, LOCAL SERVICE, NETWORK SERVICE, SERVICE'" { $currentUserRightsPolicy.SeCreateGlobalPrivilege | Should -Be @('NT AUTHORITY\LOCAL SERVICE', 'NT AUTHORITY\NETWORK SERVICE', 'BUILTIN\Administrators', 'NT AUTHORITY\SERVICE') }
        It "2.2.16 (L1) Ensure 'Create permanent shared objects' is set to 'No One'" { $currentUserRightsPolicy.SeCreatePermanentPrivilege | Should -BeNullOrEmpty }
        It "2.2.19 (L1) Ensure 'Debug programs' is set to 'Administrators'" { $currentUserRightsPolicy.SeDebugPrivilege | Should -Be @('BUILTIN\Administrators') }
        It "2.2.22 (L1) Ensure 'Deny log on as a batch job' to include 'Guests'" { $currentUserRightsPolicy.SeDenyBatchLogonRight | Should -Be @('BUILTIN\Guests') }
        It "2.2.23 (L1) Ensure 'Deny log on as a service' to include 'Guests'" { $currentUserRightsPolicy.SeDenyServiceLogonRight | Should -Be @('BUILTIN\Guests') }
        It "2.2.24 (L1) Ensure 'Deny log on locally' to include 'Guests'" { $currentUserRightsPolicy.SeDenyInteractiveLogonRight | Should -Be @('BUILTIN\Guests') }
        It "2.2.29 (L1) Ensure 'Force shutdown from a remote system' is set to 'Administrators'" { $currentUserRightsPolicy.SeRemoteShutdownPrivilege | Should -Be @('BUILTIN\Administrators') }
        It "2.2.30 (L1) Ensure 'Generate security audits' is set to 'LOCAL SERVICE, NETWORK SERVICE'" { $currentUserRightsPolicy.SeAuditPrivilege | Should -Be @('BUILTIN\Administrators', 'BUILTIN\Users', 'BUILTIN\Backup Operators') }

    }
    if ($domainController)
    {
        Context "2.2 User Rights Assignment - Domain Controller only" {
            It "2.2.2 (L1) Ensure 'Access this computer from the network' is set to 'Administrators, Authenticated Users, ENTERPRISE DOMAIN CONTROLLERS' (DC only)" { $currentUserRightsPolicy.SeNetworkLogonRight | Should -BeExactly 'Administrators, Authenticated Users, ENTERPRISE DOMAIN CONTROLLERS' }
            It "2.2.5 (L1) Ensure 'Add workstations to domain' is set to 'Administrators' (DC only)" { $currentUserRightsPolicy.SeTcbPrivilege | Should -BeIn @('Administrators') }
            It "2.2.8 (L1) Ensure 'Allow log on through Remote Desktop Services' is set to 'Administrators' (DC only)" { $currentUserRightsPolicy.SeRemoteInteractiveLogonRight | Should -BeNullOrEmpty }
            It "2.2.17 (L1) Ensure 'Create symbolic links' is set to 'Administrators' (DC only)" { $currentUserRightsPolicy.SeCreateSymbolicLinkPrivilege | Should -BeNullOrEmpty }
            It "2.2.20 (L1) Ensure 'Deny access to this computer from the network' is set to 'Guests' (DC only)" { $currentUserRightsPolicy.SeRemoteInteractiveLogonRight | Should -BeNullOrEmpty }
            It "2.2.25 (L1) Ensure 'Deny log on through Remote Desktop Services' is set to 'Guests' (DC only)" { $currentUserRightsPolicy.SeRemoteInteractiveLogonRight | Should -BeNullOrEmpty }
            It "2.2.27 (L1) Ensure 'Enable computer and user accounts to be trusted for delegation' is set to 'Administrators' (DC only)" { $currentUserRightsPolicy.SeRemoteInteractiveLogonRight | Should -BeNullOrEmpty }
        }
    }
    if ($memberServer)
    {
        Context "2.2 User Rights Assignment - Member Server (MS) only" {
            It "2.2.3 (L1) Ensure 'Access this computer from the network' is set to 'Administrators, Authenticated Users' (MS only)" { $currentUserRightsPolicy.SeNetworkLogonRight | Should -BeExactly 'Administrators, Authenticated Users' }
            It "2.2.9 (L1) Ensure 'Allow log on through Remote Desktop Services' is set to 'Administrators, Remote Desktop Users' (MS only)" { $currentUserRightsPolicy.SeNetworkLogonRight | Should -BeExactly 'Administrators, Authenticated Users' }
            It "2.2.18 (L1) Ensure 'Create symbolic links' is set to 'Administrators, NT VIRTUAL MACHINE\Virtual Machines' (MS only)" { $currentUserRightsPolicy.SeNetworkLogonRight | Should -BeExactly 'Administrators, Authenticated Users' }
            It "2.2.21 (L1) Ensure 'Deny access to this computer from the network' is set to 'Guests, Local account and member of Administrators group' (MS only)" { $currentUserRightsPolicy.SeNetworkLogonRight | Should -BeExactly 'Administrators, Authenticated Users' }
            It "2.2.26 (L1) Ensure 'Deny log on through Remote Desktop Services' is set to 'Guests, Local account' (MS only)" { $currentUserRightsPolicy.SeNetworkLogonRight | Should -BeExactly 'Administrators, Authenticated Users' }
            It "2.2.28 (L1) Ensure 'Enable computer and user accounts to be trusted for delegation' is set to 'No One' (MS only)" { $currentUserRightsPolicy.SeNetworkLogonRight | Should -BeExactly 'Administrators, Authenticated Users' }
        }
    }
}
